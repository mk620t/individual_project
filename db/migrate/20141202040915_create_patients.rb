class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.string :address
      t.string :city
      t.integer :state_id
      t.string :zip
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
