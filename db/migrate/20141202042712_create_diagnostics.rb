class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :abbreviation
      t.string :name

      t.timestamps
    end
  end
end
