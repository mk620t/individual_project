class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.date :hired

      t.timestamps
    end
  end
end
