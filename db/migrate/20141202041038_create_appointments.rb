class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.date :app_date
      t.time :app_time
      t.string :description
      t.string :notes
      t.integer :diagnostic_id
      t.float :cost

      t.timestamps
    end
  end
end
