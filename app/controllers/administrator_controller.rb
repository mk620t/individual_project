class AdministratorController < ApplicationController
  def index
    @physicians = Physician.all
    @diagnostics = Diagnostic.all
  end
end
