class Diagnostic < ActiveRecord::Base
  belongs_to :appointment
  validates :abbreviation, presence: true
  validates :name, presence: true


  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Diagnostic.create! row.to_hash
    end
  end
end

