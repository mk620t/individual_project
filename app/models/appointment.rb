class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic
  validates :app_time, uniqueness: {scope: [:app_date, :physician_id]}, presence: true
  validates :description, presence: true
end
