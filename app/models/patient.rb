class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, :through => :appointments
  belongs_to :state
  validates :name, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :zip, presence: true, length: { is: 5 }, numericality: { only_integer: true}
  validates :phone, presence: true
  validates :email, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
end
