class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, :through => :appointments
  validates :name, presence: true
  validates :email, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates :phone, presence: true
end
