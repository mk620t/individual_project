json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :app_date, :app_time, :description, :notes, :diagnostic_id, :cost
  json.url appointment_url(appointment, format: :json)
end
