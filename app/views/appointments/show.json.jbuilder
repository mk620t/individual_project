json.extract! @appointment, :id, :patient_id, :physician_id, :app_date, :app_time, :description, :notes, :diagnostic_id, :cost, :created_at, :updated_at
