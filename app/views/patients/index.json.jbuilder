json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :address, :city, :state_id, :zip, :phone, :email
  json.url patient_url(patient, format: :json)
end
