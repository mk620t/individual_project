json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :abbreviation, :name
  json.url diagnostic_url(diagnostic, format: :json)
end
